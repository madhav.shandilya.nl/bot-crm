# SfBoT Release @DX + GIT 

This guide helps Salesforce developers get started with GitLab and Salesforce development quickly including details on development models, setting up CI/CD with GitLab pipelines, and how to deploy your final changes to production.

## Part 1: Choosing a Salesforce Development Model

There are two types of developer processes or models at Salesforce. These models are explained below. Each model offers pros and cons and is fully supported.
However, this CI/CD process use the Package Development Model.
 
### Package Development Model

If you are starting a new project, we recommend that you consider the package development model. For details about the model, see the [Package Development Model](https://trailhead.salesforce.com/en/content/learn/modules/sfdx_dev_model) Trailhead module.

### Org Development Model

If you are developing against non-source-tracked orgs, use the command `SFDX: Create Project with Manifest` (VS Code) or `sfdx force:project:create --manifest` (Salesforce CLI) to create your project. If you used another command, you might want to start over with this command to create a Salesforce DX project.

## The `sfdx-project.json` File

The `sfdx-project.json` file contains useful configuration information for your project. See [Salesforce DX Project Configuration](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_ws_config.htm) in the _Salesforce DX Developer Guide_ for details about this file.

The most important parts of this file for getting started are the `sfdcLoginUrl` and `packageDirectories` properties.

```json
"packageDirectories" : [
    {
      "path": "force-app",
      "default": true
    }
]
```

## Part 2: Input Gitlab CI/CD Pipeline Environment Variables

Before you run your CI pipeline, you need to enter a few configuration variables to allow GitLab to interact with your Salesforce orgs.

1. Install [Salesforce CLI](https://developer.salesforce.com/tools/sfdxcli) on your local machine.
2. [Authenticate using the web-based oauth flow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_web_flow.htm) to each of the orgs that represent your Dev Hub, sandbox, and production org. **Note, for initial testing of GitLab CI, your "sandbox" and "production" orgs can be Trailhead Playgrounds or any Developer Edition org.**
3. Use the `sfdx force:org:display --targetusername <username> --verbose` command to get the [Sfdx Auth Url](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_view_info.htm) for your various orgs. The URL you're looking for starts with `force://`. **Note, you must use the `--verbose` argument to see the Sfdx Auth URL.**

From your GitLab project page, click **Settings**, **CI / CD**, then expand **Variables**.

![Enter CI/CD variables](images/cicd-variables.png)

Here are the Auth URL variables to set:

- `DEVHUB_AUTH_URL`
- `SANDBOX_AUTH_URL`
- `PRODUCTION_AUTH_URL` (if your prod org is the same as your Dev Hub, then this can ommitted and the DEVHUB_AUTH_URL will be used by default)

Here are some other variables that are optional:

- `DEPLOY_SCRATCH_ON_EVERY_COMMIT`: "true" to deploy a scratch org on every commit in a merge request, otherwise it won't.
- `PACKAGE_NAME`: Optional. Must match one of the `packageDirectories` entries in `sfdx-project.json`. If not present, then the CI pipeline uses the default package directory from `sfdx-project.json`.

Optionally, disable entire jobs with the following boolean variables:

- `TEST_DISABLED`
- `SCRATCH_DISABLED`
- `SANDBOX_DISABLED`
- `PRODUCTION_DISABLED`

## Part 3: Develop your App

When the CI pipeline runs, it will deploy that metadata and run those tests in a new scratch org. When you're ready to try with your own org metadata, you may merge the contents in `force-app`.

## Part 4: Create your Package

As a one-time step per CI pipeline, you need to create the unlocked package that the pipeline will create and install new versions for. TODO - CI process uses the Package Development Model & DEVHUB in production

The `sfdx-project.json` file specifies **SfBotPkg** as a default package name.
To use that default for testing, run the following command with Salesforce CLI:

```
sfdx force:package:create --name SfBotPkg --packagetype Unlocked --path force-app --targetdevhubusername <DEVHUB_ALIAS>
```

## Part 6: Run Your Pipeline

Simply commit a change to your GitLab repository and see the GitLab CI/CD pipeline kick off! Some CI jobs, like deploying to sandboxes or production orgs, require your commits and merge requests to occur on the `master` branch.

![Completed pipeline](images/completed-pipeline.png)